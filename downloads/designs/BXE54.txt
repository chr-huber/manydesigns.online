
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 >> Below, we report the treatment effect obtained for your pre-registered (primary) analysis.
 >> We only focus on the coefficient estimate of the competition/treatment indicator variable.
 >> If your analysis involves controls, they are included in the model but not reported below.
 >> The sample size (N) refers to the number of obs. after implementing your exclusion criteria.

 >> To aggregate results across teams, we align the signs of the estimates...
    -- positive coefficient: competition induces more moral (less immoral) behavior
    -- negative coefficient: competition induces more immoral (less moral) behavior
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~



~~~~~~~~~~~~~~~~~~~~~~
Competition Effect:
~~~~~~~~~~~~~~~~~~~~~~
  coef.    = -0.3547
  s.e.     =  0.0993
  t(404)   = -1.3293
  p > |t|  =  0.1845
  N        =     406
~~~~~~~~~~~~~~~~~~~~~~
