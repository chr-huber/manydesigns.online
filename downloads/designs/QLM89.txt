
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 >> Below, we report the treatment effect obtained for your pre-registered (primary) analysis.
 >> We only focus on the coefficient estimate of the competition/treatment indicator variable.
 >> If your analysis involves controls, they are included in the model but not reported below.
 >> The sample size (N) refers to the number of obs. after implementing your exclusion criteria.

 >> To aggregate results across teams, we align the signs of the estimates...
    -- positive coefficient: competition induces more moral (less immoral) behavior
    -- negative coefficient: competition induces more immoral (less moral) behavior
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~



~~~~~~~~~~~~~~~~~~~~~~
Competition Effect:
~~~~~~~~~~~~~~~~~~~~~~
  coef.    =  0.1597
  s.e.     =  0.1256
  t(186)   =  0.4037
  p > |t|  =  0.6869
  N        =     191
~~~~~~~~~~~~~~~~~~~~~~
